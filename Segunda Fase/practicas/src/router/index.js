import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/S2",
    name: "Sesion 2",
    component: () =>
      import("../views/S2/Home.vue"),
  },
  {
    path: "/S3",
    name: "Sesion 3",
    component: () =>
      import("../views/S3/Home.vue"),
  },
  {
    path: "/S4",
    name: "Sesion 4",
    component: () =>
      import("../views/S4/Home.vue"),
  },
  {
    path: "/S5",
    name: "Sesion 5",
    component: () =>
      import("../views/S5/Home.vue"),
  },
  {
    path: "/S6",
    name: "Sesion 6",
    component: () =>
      import("../views/S6/Home.vue"),
  },
  {
    path: "/PostWork1",
    name: "PostWork 1",
    component: () =>
      import("../views/PostWorks/Home.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
